var members_url = APIBASEURL.concat('/members');
var uncoveredMembers_url = members_url.concat('/uncovered');
var insurance_url = members_url.concat('/cover');

function registerMember(chapter, firstname, lastname, email, phone, gender, date_of_birth, password) {
  let requestObj = {
    school_alias: chapter,
    surname: lastname,
    firstname: firstname,
    email: email,
    phone: phone,
    gender_id: gender,
    date_of_birth: date_of_birth,
    password: password
  };

  $.post(members_url, requestObj)
    .done(() => {
      $('#regFormContainer').prepend('<div class="alert alert-success" role="alert"><div class="container"><div class="alert-icon"><i class="now-ui-icons ui-2_like"></i></div><strong>Well done!</strong> Your registration was successful. Kindly check your email.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="now-ui-icons ui-1_simple-remove"></i></span></button></div></div>');
      setTimeout(() => {
        location.reload();
      }, 3000);
    })
    .fail((error) => {
      console.error(error);
      $('#memberSignup').attr('disabled', false).attr('aria-disabled', false);
      $('#regIcon').addClass('ui-1_send').removeClass('loader_refresh spin');
      $('#regFormContainer').prepend('<div class="alert alert-danger" role="alert"><div class="container"><div class="alert-icon"><i class="now-ui-icons ui-2_dislike"></i></div><strong>Oops!</strong> Your registration was not successful.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="now-ui-icons ui-1_simple-remove"></i></span></button></div></div>');
      setTimeout(() => {
        $('.alert.alert-danger').remove();
      }, 3000);
    });
}

function fetchUncoveredMembers() {
  $('#query').attr('placeholder', 'Loading! Please wait...').attr('disabled', 'disabled');
  $("#searchContainer").attr('disabled', 'disabled');
  $('#searchIcon').addClass('loader_refresh spin').removeClass('ui-1_zoom-bold');

  fetch(uncoveredMembers_url)
    .then((response) => {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' + response.status);
        return;
      }

      response.json().then((data) => {
        let newMembers = data.uncoveredMembers.map(member => {
          return {
            'id': member.mrn,
            'name': member.firstName + ' ' + member.otherName + ' ' + member.surname + ' (' + member.mrn + ')',
            'memberDetails': member
          };
        });
        $("#query").typeahead({
          source: newMembers
        });
        $('#query').attr('placeholder', 'Start typing member\'s name or MRN to search...').attr('disabled', false);
        $("#searchContainer").attr('disabled', false);
        $('#searchIcon').removeClass('loader_refresh spin').addClass('ui-1_zoom-bold');
      });
    })
    .catch((err) => {
      console.error('Fetch Error :-S', err);
    });
}

function populateInsuranceForm(memberDetails) {
  var mrn = memberDetails.mrn;
  var firstName = memberDetails.firstName;
  var otherName = memberDetails.otherName || '';
  var surname = memberDetails.surname;
  var phone = memberDetails.phone;
  var email = memberDetails.email;

  $('#memberInsuranceForm').attr('hidden', false);
  $('#memberRegNumber').val(mrn);
  $('#firstName').val(firstName);
  $('#otherName').val(otherName);
  $('#surname').val(surname);
  $('#memberEmail').val(email);
  $('#memberPhone').val(phone);
}

function insureMember(memberDetails) {
  $.post(insurance_url, memberDetails)
    .done(() => {
      $('#memberInsuranceFormContainer').prepend('<div class="alert alert-success" role="alert"><div class="container"><div class="alert-icon"><i class="now-ui-icons ui-2_dislike"></i></div><strong>Congrats!</strong> You are now registered for the Welfare Scheme.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="now-ui-icons ui-1_simple-remove"></i></span></button></div></div>');
      setTimeout(() => {
        location.reload();
      }, 5000);
      $('#memberInsurancePay').attr('disabled', false).attr('aria-disabled', false);
      $('#paymentIcon').addClass('ui-1_send').removeClass('loader_refresh spin');
    })
    .fail((err) => {
      console.error(err);
    });
}