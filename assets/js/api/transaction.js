var transaction_url = APIBASEURL.concat('/transactions');

function gladePayInitiateDuesPayment(chapterDetails) {
  initPayment({
    MID: 'GP_YR8DO9mhqlvt6UlF5ERAZZCcjPvF0nsn',
    amount: $('#amount').val(),
    email: chapterDetails.chapterEmail,
    firstname: chapterDetails.schoolAlias,
    lastname: chapterDetails.chapterName,
    phone: chapterDetails.chapterPhone,
    title: 'Annual Chapter Dues',
    description: 'NACOSS Annual Chapter Dues Payment',
    payment_method: ['card', 'bank', 'ussd', 'qr'],
    logo: 'https://nacoss.org.ng/assets/img/nat-logo-small.png',
    onclose: () => {
      $('#chapterDuesPay').attr('disabled', false).attr('aria-disabled', false);
      $('#paymentIcon').addClass('ui-1_send').removeClass('loader_refresh spin');
    },
    callback: (response) => {
      if (response.status === 200) {
        logTransaction({
          email: response.email,
          phone: chapterDetails.chapterPhone,
          transactionRef: response.txnRef,
          responseCode: '00',
          responseMessage: 'Payment Successful',
          purpose: 'chapter_dues'
        });
        
        setTimeout(() => {
          activateChapter(chapterDetails.chapterName, response.txnRef);
        }, 3000);
      } else {
        var element = document.getElementById('query');
        var event = new Event('change');
        element.dispatchEvent(event);
      }
    }
  });
}

function gladePayInitiateChapterRegistrationPayment(chapterDetails) {
  initPayment({
    MID: 'GP_YR8DO9mhqlvt6UlF5ERAZZCcjPvF0nsn',
    amount: $('#amount').val(),
    email: chapterDetails.chapterEmail,
    firstname: chapterDetails.schoolAlias,
    lastname: chapterDetails.chapterName,
    phone: chapterDetails.chapterPhone,
    description: 'NACOSS Chapter Registration Payment',
    payment_method: ['card', 'bank', 'ussd', 'qr'],
    logo: 'https://nacoss.org.ng/assets/img/nat-logo-small.png',
    onclose: () => {
      $('#chapterRegistrationPay').attr('disabled', false).attr('aria-disabled', false);
      $('#paymentIcon').addClass('ui-1_send').removeClass('loader_refresh spin');
    },
    callback: (response) => {
      if (response.status === 200) {
        const chapterObj = {
          chapterEmail: response.email,
          phone: chapterDetails.chapterPhone,
          transactionRef: response.txnRef,
          responseCode: '00',
          responseMessage: 'Payment Successful',
          chapterName: chapterDetails.chapterName,
          purpose: 'chapter_reg'
        };

        logTransaction({
          email: response.email,
          phone: chapterDetails.chapterPhone,
          transactionRef: response.txnRef,
          responseCode: '00',
          responseMessage: 'Payment Successful',
          purpose: 'chapter_reg'
        });
        
        setTimeout(() => {
          addChapter(chapterDetails, chapterObj);
        }, 3000);
      } else {
        var element = document.getElementById('query');
        var event = new Event('change');
        element.dispatchEvent(event);
      }
    }
  });
}

function logTransaction(logObj) {
  let requestObj = {
    email: logObj.email,
    phone: logObj.phone,
    transaction_ref: logObj.transactionRef,
    response_code: logObj.responseCode,
    response_message: logObj.responseMessage,
    purpose_id: logObj.purpose
  };

  $.post(transaction_url, requestObj)
    .done(function () {
      alert('Transaction logged with NACOSS Successfully!');
    })
    .fail(function (err) {
      console.log(err);
      $('#chapterDuesFormContainer', '#chapterRegFormContainer').prepend('<div class="alert alert-danger" role="alert"><div class="container"><div class="alert-icon"><i class="now-ui-icons ui-2_dislike"></i></div><strong>Server Error!</strong> Please try again later.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="now-ui-icons ui-1_simple-remove"></i></span></button></div></div>');
      setTimeout(function () {
        $('.alert.alert-danger').remove();
      }, 10000);
      $('#chapterDuesPay', '#chapterRegistrationPay').attr('disabled', false).attr('aria-disabled', false);
      $('#paymentIcon').addClass('ui-1_send').removeClass('loader_refresh spin');
    });
}

function gladePayInitiateInsurancePayment(memberDetails) {
  initPayment({
    MID: 'GP_YR8DO9mhqlvt6UlF5ERAZZCcjPvF0nsn',
    amount: $('#amount').val(),
    email: memberDetails.email,
    firstname: memberDetails.firstName,
    lastname: memberDetails.surname,
    phone: memberDetails.phone,
    title: 'Member Welfare Scheme',
    description: 'NACOSS Annual Member Welfare Scheme Payment',
    payment_method: ['card', 'bank', 'ussd', 'qr'],
    logo: 'https://nacoss.org.ng/assets/img/nat-logo-small.png',
    onclose: () => {
      $('#chapterDuesPay').attr('disabled', false).attr('aria-disabled', false);
      $('#paymentIcon').addClass('ui-1_send').removeClass('loader_refresh spin');
    },
    callback: (response) => {
      if (response.status === 200) {
        logTransaction({
          email: response.email,
          phone: memberDetails.phone,
          transactionRef: response.txnRef,
          responseCode: '00',
          responseMessage: 'Payment Successful',
          purpose: 'welfare_scheme'
        });
        
        setTimeout(() => {
          insureMember({
            mrn: memberDetails.mrn,
            beneficiary_name: memberDetails.nokName,
            beneficiary_phone: memberDetails.nokPhone,
            cover_year: new Date().getFullYear()
          });
        }, 3000);
      } else {
        var element = document.getElementById('query');
        var event = new Event('change');
        element.dispatchEvent(event);
      }
    }
  });
}