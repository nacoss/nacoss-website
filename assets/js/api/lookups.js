var lookup_url = APIBASEURL.concat('/lookups');
var genger_lookup = lookup_url.concat('/gender');
var zone_lookup = lookup_url.concat('/zones');

function fetchGender() {
  fetch(genger_lookup)
    .then((response) => {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' + response.status);
        return;
      }

      response.json().then((data) => {
        var $genderSelect = $('#gender');
        $.each(data.gender, (key, value) => {
          $genderSelect.append('<option value=' + value.genderId + '>' + value.genderName + '</option>');
        });
      });
    })
    .catch((err) => {
      console.log('Fetch Error :-S', err);
    });
}

function fetchZones() {
  fetch(zone_lookup)
    .then((response) => {
      if (response.status !== 200) {
        console.error('Looks like there was a problem. Status code: ' + response.status);
        return;
      }

      response.json().then((data) => {
        var $zoneSelect = $('#chapterZone');
        $.each(data.zones, (key, value) => {
          $zoneSelect.append('<option value=' + value.zoneId + '>' + value.zoneName + '</option>');
        });
      });
    })
    .catch((err) => {
      console.error('Fetch Error :-S', err);
    });
}