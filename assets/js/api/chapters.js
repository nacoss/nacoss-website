var chapter_url = APIBASEURL.concat('/chapters');
var active_chapters = chapter_url.concat('/active');
var inactive_chapters = chapter_url.concat('/inactive');
var activate_chapter = chapter_url.concat('/activate');
var search_chapter = chapter_url.concat('/search');
var complete_chapter_reg = chapter_url.concat('/complete');

function fetchInactiveChapters() {
  $('#query').attr('placeholder', 'Loading! Please wait...').attr('disabled', 'disabled');
  $("#searchContainer").attr('disabled', 'disabled');
  $('#searchIcon').addClass('loader_refresh spin').removeClass('ui-1_zoom-bold');

  fetch(inactive_chapters)
    .then((response) => {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' + response.status);
        return;
      }

      response.json().then((data) => {
        var $chapterSelect = $('#chapter');
        $.each(data.inActiveChapters, (key, value) => {
          $chapterSelect.append('<option value=' + value.schoolAlias + '>' + value.schoolName + ' (' + value.chapterName + ')</option>');
        });

        let newChapters = data.inActiveChapters.map(chapter => {
          return {
            'id': chapter.schoolAlias,
            'name': chapter.schoolName + '  (' + chapter.chapterName + ')',
            'chapterDetails': chapter
          };
        });
        $("#query").typeahead({
          source: newChapters
        });
        $('#query').attr('placeholder', 'Start typing school name to search...').attr('disabled', false);
        $("#searchContainer").attr('disabled', false);
        $('#searchIcon').removeClass('loader_refresh spin').addClass('ui-1_zoom-bold');
      });
    })
    .catch((err) => {
      console.error('Fetch Error :-S', err);
    });
}

function fetchActiveChapters() {
  fetch(active_chapters)
    .then((response) => {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' + response.status);
        return;
      }

      response.json().then((data) => {
        if (data.activeChapters.length < 1) {
          $("#regFormContainer").prepend('<div class="alert alert-danger" role="alert"><div class="container"><strong>Oops!</strong> No local chapter has paid the annual dues.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="now-ui-icons ui-1_simple-remove"></i></span></button></div></div>');
          $('form').remove();
          return;
        }

        var $chapterSelect = $('#chapter');
        $.each(data.activeChapters, (key, value) => {
          $chapterSelect.append('<option value=' + value.schoolAlias + '>' + value.schoolName + ' (' + value.chapterName + ')</option>');
        });
        $('.alert.alert-info').remove();
        $('#regForm').attr('hidden', false);
      });
    })
    .catch((err) => {
      console.error('Fetch Error :-S', err);
    });
}

function populateDuesForm(chapterDetails) {
  $('#chapterDuesForm').attr('hidden', false);
  $('#chapterName').val(chapterDetails.chapterName);
  $('#chapterRegNumber').val(chapterDetails.chapterRegistrationNumber);
  $('#chapterZone').val(chapterDetails.zone);
  $('#chapterEmail').val(chapterDetails.chapterEmail);
  $('#chapterPhone').val(chapterDetails.chapterPhone);
}

function computeAmount() {
  $('#amount').attr('disabled', false);
  let amount = $('#amount').val();
  if (document.getElementById('insurance').checked) {
    if (amount == 10000) {
      amount = 15000;
      $('#amount').val(amount);
    } else if (amount == 20000) {
      amount = 25000;
      $('#amount').val(amount);
    }
  } else {
    if (amount == 15000) {
      amount = 10000;
      $('#amount').val(amount);
    } else if (amount == 25000) {
      amount = 20000;
      $('#amount').val(amount);
    }
  }
  $('#amount').attr('disabled', true);
}

function activateChapter(chapterName, transactionRef) {
  var requestObj = {
    chapter_name: chapterName,
    transaction_ref: transactionRef
  };

  $.post(activate_chapter, requestObj)
    .done(() => {
      alert('Chapter dues payment confirmed and chapter activation successful!');
      location.href = './';
    })
    .fail(() => {
      $('#chapterDuesFormContainer').prepend('<div class="alert alert-danger" role="alert"><div class="container"><div class="alert-icon"><i class="now-ui-icons ui-2_dislike"></i></div><strong>Server Error!</strong> Please try again later.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="now-ui-icons ui-1_simple-remove"></i></span></button></div></div>');
      setTimeout(function () {
        $('.alert.alert-danger').remove();
      }, 10000);
      $('#chapterDuesPay').attr('disabled', false).attr('aria-disabled', false);
      $('#paymentIcon').addClass('ui-1_send').removeClass('loader_refresh spin');
    });
}

function searchChapter(searchTerm) {
  $.get(search_chapter.concat('/').concat(searchTerm))
    .done((response) => {
      if (response.chapters.length === 0) {
        $('#searchContainer').addClass('has-success').removeClass('has-danger');
        $('#schoolAlias').addClass('form-control-success').removeClass('form-control-danger');
        $('#searchIcon').removeClass('loader_refresh spin ui-1_simple-remove'); //.addClass('ui-1_check');
        populateChapterName(searchTerm);
      } else {
        $('#searchContainer').addClass('has-danger').removeClass('has-success');
        $('#schoolAlias').removeClass('form-control-success').addClass('form-control-danger');
        $('#searchIcon').removeClass('loader_refresh spin ui-1_check'); //.addClass('ui-1_simple-remove');
        $('#chapterRegistrationForm').attr('hidden', true);
        $('#chapterName').attr('readonly', false).val('').attr('readonly', true);
      }
    })
    .fail((err) => {
      console.error(err);
    });
}

function populateChapterName(schoolAlias) {
  var chapterName = 'NACOSS'.concat(schoolAlias.toUpperCase());
  $('#chapterRegistrationForm').attr('hidden', false);
  $('#chapterName').attr('readonly', false).val(chapterName).attr('readonly', true);
}

function addChapter(chapterDetails, chapterObj) {
  let requestObj = {
    transaction_ref: chapterObj.transactionRef,
    chapter_email: chapterDetails.chapterEmail,
    chapter_name: chapterDetails.chapterName
  };

  $.post(chapter_url, requestObj)
    .done(() => {
      completeChapterRegistration(chapterDetails, chapterObj);
    })
    .fail((err) => {
      console.error(err);
    });
}

function completeChapterRegistration(chapterDetails, chapterObj) {
  let requestObj = {
    school_alias: chapterDetails.schoolAlias,
    school_name: chapterDetails.schoolName,
    chapter_name: chapterDetails.chapterName,
    zone_id: chapterDetails.chapterZone,
    chapter_email: chapterDetails.chapterEmail,
    address: chapterDetails.chapterAddress,
    hod_name: chapterDetails.hodName,
    hod_phone: chapterDetails.hodPhone,
    transaction_ref: chapterObj.transactionRef,
  };

  $.post(complete_chapter_reg, requestObj)
    .done(() => {
      alert('Congratulations! Chapter Registration complete.');
      location.href = './chapter-dues.html';
    })
    .fail(() => {
      console.error(err);
    });
}